import static org.junit.Assert.*;

import java.util.EmptyStackException;

import ku.util.*;

import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

/**
 * A test class that test a stack by using all of the test case.
 * @author Narut Poovorakit
 * @version 31.03.2015
 */
public class StackTest {
	/** Create a stack  */
	private Stack stack;

	@Before
	public void setUp() throws Exception {
		StackFactory.setStackType(0);
		stack = StackFactory.makeStack(3);

	}

	/** isEmpty method
	 *  test if the stack is empty or not. */
	
	@Test
	public void testStackIsEmpty() {
		assertTrue(stack.isEmpty());
		assertFalse(stack.isFull());
		assertEquals(0, stack.size());

	}

	/** IsFull method 
	 *  test if the stack is full or not. */
	@Test
	public void testStackIsFull() {
		stack = StackFactory.makeStack(1);
		stack.push(3);
		assertTrue(stack.isFull());
	}

	/** Peek method 
	 *  test if when peek the stack. */
	@Test
	public void testStackPeekIsNull() {
		stack = StackFactory.makeStack(1);
		stack.push(1);
		assertEquals(1, stack.peek());
	}

	/** Pop method 
	 *  test if the stack is empty and pop it out. */
	@Test(expected = EmptyStackException.class)
	public void testStackPop() {
		Assume.assumeTrue(stack.isEmpty());
		stack.pop();
	}

	/** Push method
	 *  test if when push null in the stack.  */
	@Test(expected = IllegalArgumentException.class)
	public void testStackPushIsNull() {
		stack = StackFactory.makeStack(2);
		stack.push(null);
	}

	/** test if the stack is push when it already full. */
	@Test(expected = IllegalStateException.class)
	public void testStackPushIsFull() {
		stack = StackFactory.makeStack(1);
		stack.push("FirstPush");
		// stack is now full
		stack.push("SecondPush");
	}

	/** test if the capacity is a negative number. */
	 
	@Test(expected = IllegalArgumentException.class)
	public void testStackIsNegativeCapacity() {
		stack = StackFactory.makeStack(-1);
		assertEquals(-1, stack.capacity());
	}

	/** test if the size of the stack is equal to the capacity of the stack. */
	@Test
	public void testStackCheckSize() {
		stack = StackFactory.makeStack(2);
		stack.push("FirstPush");
		stack.push("SecondPush");
		assertEquals(stack.size(), stack.capacity());
	}

}
